import React from 'react'
import {Route, Switch} from 'react-router-dom'
import AddProduct from './AddProduct'
import AdminLogin from './AdminLogin'
const MainRouter = () => {
return ( <div>
<Switch>
<Route exact path="/" component={AddProduct}/>
<Route path="/adminlogin" component={AdminLogin}/>
</Switch>
</div>
)
}
export default MainRouter