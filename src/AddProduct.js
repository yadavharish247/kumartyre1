import React from 'react'
import './AddProduct.css'
import imgBtn  from './Assets/add.png'
export default function AddProduct(){

  const hiddenFileInput = React.useRef(null);
  
  // Programatically click the hidden file input element
  // when the Button component is clicked
  const handleClick = event => {
    hiddenFileInput.current.click();
  };  // Call a function (passed as a prop from the parent component)
  // to handle the user-selected file 
  const handleChange = event => {
    const fileUploaded = event.target.files[0];
    
  };

  
    return (

                <div>
                  <h1 id="title">Add Product</h1>
                  <div id="form-container">
                    <form>
                      <div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Name: </label>
                        </div>
                        <div className="rightTab">
                          <input autofocus type="text" name="name" id="name" className="input-field" placeholder="Enter your full name" required />
                        </div>
                      </div>
                      <div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Vehicle Name: </label>
                        </div>
                        <div className="rightTab">
                          <input autofocus type="text" name="name" id="name" className="input-field" placeholder="Enter vehicle name" required />
                        </div>
                      </div><div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Size: </label>
                        </div>
                        <div className="rightTab">
                          <input autofocus type="text" name="name" id="name" className="input-field" placeholder="Enter size" required />
                        </div>
                      </div><div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Warranty: </label>
                        </div>
                        <div className="rightTab">
                          <input autofocus type="text" name="name" id="name" className="input-field" placeholder="Enter warranty period" required />
                        </div>
                      </div>
                     
                      <div className="rowTab">
                        <div className="labels">
                          <label htmlFor="userRating">Select type of product:</label>
                        </div>
                        <div className="rightTab">
                          <ul style={{listStyle: 'none'}}>
                            <li className="radio"><label>Tyre<input name="radio-buttons" defaultValue={1} type="radio" className="userRatings" /></label></li>
                            <li className="radio"><label>Tube<input name="radio-buttons" defaultValue={2} type="radio" className="userRatings" /></label></li>
                          </ul>
                        </div>
                      </div>
                      <div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Company: </label>
                        </div>
                        <div className="rightTab">
                          <input autofocus type="text" name="name" id="name" className="input-field" placeholder="Enter product company name" required />
                        </div>
                      </div>

                     
                     
                      <div className="rowTab">
                        <div className="labels">
                          <label htmlFor="comments">Description:</label>
                        </div>
                        <div className="rightTab">
                          <textarea id="comments" className="input-field" style={{height: '50px', resize: 'vertical'}} name="comment" placeholder="Enter your comment here..." defaultValue={""} />
                        </div>
                      </div>
                      <div className="rowTab">
                        <div className="labels">
                          <label id="name-label" htmlFor="name">Product Images: </label>
                        </div>
                        <div className="rightTab">
                        <button className="imgBtn"><img className="imgSize" src= { imgBtn } alt="my image" onClick={handleClick} /></button> 
                        <input type="file" ref={hiddenFileInput} onChange={handleChange} style={{display: 'none'}}  />
                                               </div>
                      </div>
                      <div id="wrapper">
                        <button id="submit" type="submit">Add</button> </div>
                    </form>
                  </div>
                </div>
              );
            }
          